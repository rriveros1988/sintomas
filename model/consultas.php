<?php
	 // ini_set('display_errors', 'On');
	require('conexion.php');

	function ingresaSintomas($rut,$mail,$fono,$temperatura,$tiempoIngreso,$foto){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "INSERT INTO PERSONAL_INGS
(RUT, EMAIL, TELEFONO, TEMPERATURA, TIPO, FECHAHORA, FOTO)
VALUES('{$rut}', '{$mail}', '{$fono}', {$temperatura}, '{$tiempoIngreso}', NOW(), '{$foto}')";
			if ($con->query($sql)) {
					return $con;
			}
			else{
				//return $con->error;
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}

	function ingresaSintomasSinFoto($rut,$mail,$fono,$temperatura,$tiempoIngreso){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "INSERT INTO PERSONAL_INGS
(RUT, EMAIL, TELEFONO, TEMPERATURA, TIPO, FECHAHORA)
VALUES('{$rut}', '{$mail}', '{$fono}', {$temperatura}, '{$tiempoIngreso}', NOW())";
			if ($con->query($sql)) {
					return $con;
			}
			else{
				//return $con->error;
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}

	function ingresaSintomasMulti($id,$id_sintoma){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "INSERT INTO rriveros_sintomas.PERSONAL_INGS_SINTOMAS
(IDPERSONAL_INGS, IDSINTOMAS)
VALUES({$id}, {$id_sintoma})";
			if ($con->query($sql)) {
					return $con;
			}
			else{
				//return $con->error;
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}

	//Login
	function consultasAcceso($rut){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT P.DNI,
initCap(CONCAT(P.NOMBRES,' ',P.APELLIDOS)) 'NOMBRE', P.FONO 'FONO', P.MAIL 'CORREO',
J.DNI 'DNI_JEFE', initCap(CONCAT(J.NOMBRES,' ',J.APELLIDOS)) 'NOMBRE_JEFE',
 RIGHT(DATE_FORMAT(NOW() , \"%r\"),2) 'HH',
CASE WHEN  G.TIPO IS NULL THEN 0 ELSE 1 END 'REGISTRO'
FROM PERSONAL P
LEFT JOIN PERSONAL J
ON P.RUTJEFEDIRECTO = J.DNI
LEFT JOIN PERSONAL_INGS G
ON P.DNI = G.RUT
AND RIGHT(DATE_FORMAT(NOW() , \"%r\"),2) = G.TIPO
AND DATE_FORMAT(NOW() , \"%y-%m-%d\") = DATE_FORMAT(G.FECHAHORA, \"%y-%m-%d\")
WHERE P.DNI = '{$rut}'
GROUP BY P.DNI,
initCap(CONCAT(P.NOMBRES,' ',P.APELLIDOS)), P.FONO, P.MAIL, J.DNI, initCap(CONCAT(J.NOMBRES,' ',J.APELLIDOS)),
G.TIPO";
			if ($row = $con->query($sql)) {
				$return = array();
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function selectSintomasCorreo($id){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT
  E.DNI 'RUT', initCap(CONCAT(E.NOMBRES,' ',E.APELLIDOS)) 'NOMBRE', P.TEMPERATURA 'TEMP',
  CASE WHEN SUM(CASE WHEN S.IDSINTOMAS = 1 THEN 1 ELSE 0 END) = 1 THEN 'SI' ELSE 'NO' END 'TOS',
  CASE WHEN SUM(CASE WHEN S.IDSINTOMAS = 2 THEN 1 ELSE 0 END) = 1 THEN 'SI' ELSE 'NO' END 'DOL_GAR',
  CASE WHEN SUM(CASE WHEN S.IDSINTOMAS = 3 THEN 1 ELSE 0 END) = 1 THEN 'SI' ELSE 'NO' END 'DOL_MUSC',
  CASE WHEN SUM(CASE WHEN S.IDSINTOMAS = 4 THEN 1 ELSE 0 END) = 1 THEN 'SI' ELSE 'NO' END 'DIF_RESPIRAR',
  CASE WHEN SUM(CASE WHEN S.IDSINTOMAS = 5 THEN 1 ELSE 0 END) = 1 THEN 'SI' ELSE 'NO' END 'SEC_NASAL',
  CASE WHEN SUM(CASE WHEN S.IDSINTOMAS = 6 THEN 1 ELSE 0 END) = 1 THEN 'SI' ELSE 'NO' END 'MAN_PIEL',
  CASE WHEN SUM(CASE WHEN S.IDSINTOMAS = 7 THEN 1 ELSE 0 END) = 1 THEN 'SI' ELSE 'NO' END 'DOLOR_CAB',
  CASE WHEN SUM(CASE WHEN S.IDSINTOMAS = 8 THEN 1 ELSE 0 END) = 1 THEN 'SI' ELSE 'NO' END 'NAU_VOM',
  CASE WHEN SUM(CASE WHEN S.IDSINTOMAS = 9 THEN 1 ELSE 0 END) = 1 THEN 'SI' ELSE 'NO' END 'DOLOR_ART',
  J.DNI 'DNI_JEFE', initCap(CONCAT(J.NOMBRES,' ',J.APELLIDOS)) 'NOMBRE_JEFE', J.MAIL 'MAIL_JEFE', J.FONO 'FONO_JEFE',
	P.TELEFONO 'FONO'
FROM
	PERSONAL E
	LEFT JOIN PERSONAL_INGS P
	ON E.DNI = P.RUT
 	LEFT JOIN PERSONAL_INGS_SINTOMAS S
 	ON P.IDPERSONAL_INGS = S.IDPERSONAL_INGS
 	LEFT JOIN PERSONAL J
 	ON E.RUTJEFEDIRECTO = J.DNI
WHERE
  P.IDPERSONAL_INGS = {$id}
GROUP BY
	E.DNI, initCap(CONCAT(E.NOMBRES,' ',E.APELLIDOS)),
	J.DNI, initCap(CONCAT(J.NOMBRES,' ',J.APELLIDOS)), J.MAIL, J.FONO, P.TELEFONO


	";
			if ($row = $con->query($sql)) {
				$return = array();
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaSintomas(){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT *
FROM SINTOMAS
WHERE VIRUS = 'Coronavirus'";
			if ($row = $con->query($sql)) {
				$return = array();
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function selectPersonalEncargadoSintomas(){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT MAIL, TIPO
FROM PERSONAL_INGS_ENCARGADOS";
			if ($row = $con->query($sql)) {
				$return = array();
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function checkUsuario($rut, $pass){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT NOMBRE, RUT, IDPERFIL, ESTADO
			FROM USUARIO
			WHERE RUT = '" . $rut . "' AND PASS = '" . $pass . "'";
			if ($row = $con->query($sql)) {

				$array = $row->fetch_array(MYSQLI_BOTH);

				return $array;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	//Refresh
	function checkUsuarioSinPass($rut){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT NOMBRE, RUT, IDPERFIL, ESTADO
			FROM USUARIO
			WHERE RUT = '" . $rut . "'";
			if ($row = $con->query($sql)) {

				$array = $row->fetch_array(MYSQLI_BOTH);

				return $array;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	//Ingresa Token Login
	function actualizaTokenLogin($rut, $token){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "UPDATE USUARIO
SET TOKEN_WEB = '{$token}'
WHERE RUT = '{$rut}'";
			if ($con->query($sql)) {
			    $con->query("COMMIT");
			    return "Ok";
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}

	//Chequeo Token
	function checkToken($token){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT RUT
FROM USUARIO
WHERE TOKEN_WEB  = '{$token}'";
			if ($row = $con->query($sql)) {

				$array = $row->fetch_array(MYSQLI_BOTH);

				return $array;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	//Borra Token Login
	function borraTokenLogin($rut){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "UPDATE USUARIO
SET TOKEN_WEB = NULL
WHERE RUT = '{$rut}'";
			if ($con->query($sql)) {
			    $con->query("COMMIT");
			    return "Ok";
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}

	//Areas comunes
	function consultaAreasComunes(){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT NOMBRE, PADRE, TIPO
	FROM AREAWEB
	WHERE TIPO = 1";
			if ($row = $con->query($sql)) {
				$return = array();
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	//Areas comunes
	function consultaAreas(){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT NOMBRE, PADRE, TIPO
	FROM AREAWEB";
			if ($row = $con->query($sql)) {
				$return = array();
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	//Areas comunes
	function consultaPersonal($rutUser){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "CALL SUBORDINADOS('{$rutUser}')";
			if ($row = $con->query($sql)) {
				$return = array();
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	//Borra registro previo
	function borraRegistroPrevio($rut,$tipo){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "DELETE FROM PERSONAL_INGS_SINTOMAS
WHERE IDPERSONAL_INGS IN
(
	SELECT G.IDPERSONAL_INGS
	FROM PERSONAL_INGS G
	WHERE DATE_FORMAT(G.FECHAHORA, \"%y-%m-%d\") = DATE_FORMAT(NOW() , \"%y-%m-%d\")
	AND G.TIPO = '{$tipo}'
	AND G.RUT  = '{$rut}'
)";
			if ($con->query($sql)) {
			    $con->query("COMMIT");
			    return "Ok";
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}

	function borraRegistroPrevio2($rut,$tipo){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "DELETE FROM PERSONAL_INGS
WHERE DATE_FORMAT(FECHAHORA, \"%y-%m-%d\") = DATE_FORMAT(NOW() , \"%y-%m-%d\")
AND TIPO = '{$tipo}'
AND RUT  = '{$rut}'";
			if ($con->query($sql)) {
			    $con->query("COMMIT");
			    return "Ok";
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}

?>
