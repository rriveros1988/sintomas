//Recarga
$(window).on("load",function (event) {
  event.preventDefault();
  event.stopImmediatePropagation();
  $("#modalAlertasSplash").modal({backdrop: 'static', keyboard: false});

  if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
      $(".navbar-brand").css("height","40pt");
      $(".navbar-toggle").fadeIn();
  }
  else{
    $(".navbar-brand").css("height","65pt");
    $("#login").height($(window).height() - 150);
  }
  setTimeout(function(){
      $('#contenido').fadeIn();
      $('#menu-lateral').fadeIn();
      $('#footer').fadeIn();
      setTimeout(function(){
        $('#modalAlertasSplash').modal('hide');
      },500);
      setTimeout(function(){
        $("#loginSystem").show("slide", {direction: "up"}, 800);
      },1000);
  },1500);
  window.location.href = "#/home";
});

//Login a sistema
$("#loginSystem-submit").unbind('click').click(function(){
  var a = Rut();
  if(a === true){
    var URLactual = window.location;
    var parametros = {
      "rutUser" : $("#loginSystem-rut").val().replace('.','').replace('.',''),
    };
    $.ajax({
        data:  parametros,
        url:   'controller/datosAcceso.php',
        type:  'post',
        beforeSend: function(){
            $("#buttonAceptarAlerta").css("display","none");
            $('#modalAlertasSplash').modal({backdrop: 'static', keyboard: false});
            $("#textoModalSplash").html("<img src='view/img/loading.gif' class='splash_charge_logo'><font style='font-size: 12pt;'>Cargando</font>");
            $('#modalAlertasSplash').modal('show');
        },
        success:  function (response) {
          var p = jQuery.parseJSON(response);
          if(p.aaData.length !== 0){
            console.log(p.aaData[0].REGISTRO);
            if(p.aaData[0].REGISTRO == 1 && $("#loginSystem-rut").val().replace('.','').replace('.','') != '13913245-9'){
              setTimeout(function(){
                $('#modalAlertasSplash').modal('hide');
              },500);
              $("#buttonAceptarAlerta").css("display","inline");
              $("#textoModal").html("Estimado usuario, ud ya realizo el ingreso de datos <b>" + p.aaData[0].HH + "</b>");
              $('#modalAlertas').modal('show');
            }
            else{
              window.location.href = "#/logged";
              $("#logoMenu").fadeIn();
            }
          }
          else{
            setTimeout(function(){
              $('#modalAlertasSplash').modal('hide');
            },500);
            $("#buttonAceptarAlerta").css("display","inline");
            $("#textoModal").html("Estimado usuario, su rut no se encuentra registrado en sistema");
            $('#modalAlertas').modal('show');
          }
        }
    });
  }
  else{
    setTimeout(function(){
      $('#modalAlertasSplash').modal('hide');
    },500);
    $("#buttonAceptarAlerta").css("display","inline");
    $('#modalAlertas').modal({backdrop: 'static', keyboard: false});
    $("#textoModal").html("Los datos ingresados no corresponden a un rut válido");
    $('#modalAlertas').modal('show');
  }
});

//Verificar input quita borde rojo
$("#loginSystem-rut").on('input', function(){
  $(this).css("border","");
});

//Verifica rut
function Rut()
{
  var texto = window.document.getElementById("loginSystem-rut").value;
  var tmpstr = "";
  for ( i=0; i < texto.length ; i++ )
    if ( texto.charAt(i) != ' ' && texto.charAt(i) != '.' && texto.charAt(i) != '-' )
      tmpstr = tmpstr + texto.charAt(i);
  texto = tmpstr;
  largo = texto.length;

  if(texto == ""){
    return false;
  }
  else if ( largo < 2 )
  {
    $("#buttonAceptarAlerta").css("display","inline");
    $("#textoModal").html("Debe ingresar el rut completo");
    $('#modalAlertas').modal('show');
    window.document.getElementById("loginSystem-rut").value = "";
    $("#loginSystem-rut").css("border","1px solid red");
    window.document.getElementById("loginSystem-rut").focus();
    window.document.getElementById("loginSystem-rut").select();
    return false;
  }

  for (i=0; i < largo ; i++ )
  {
    if ( texto.charAt(i) !="0" && texto.charAt(i) != "1" && texto.charAt(i) !="2" && texto.charAt(i) != "3" && texto.charAt(i) != "4" && texto.charAt(i) !="5" && texto.charAt(i) != "6" && texto.charAt(i) != "7" && texto.charAt(i) !="8" && texto.charAt(i) != "9" && texto.charAt(i) !="k" && texto.charAt(i) != "K" )
    {
      $("#buttonAceptarAlerta").css("display","inline");
      $("#textoModal").html("Los datos ingresados no corresponden a un rut válido");
      $('#modalAlertas').modal('show');
      window.document.getElementById("loginSystem-rut").value = "";
      $("#loginSystem-rut").css("border","1px solid red");
      window.document.getElementById("loginSystem-rut").focus();
      window.document.getElementById("loginSystem-rut").select();
      return false;
    }
  }

  var invertido = "";
  for ( i=(largo-1),j=0; i>=0; i--,j++ )
    invertido = invertido + texto.charAt(i);
  var dtexto = "";
  dtexto = dtexto + invertido.charAt(0);
  dtexto = dtexto + '-';
  cnt = 0;

  for ( i=1,j=2; i<largo; i++,j++ )
  {
    //alert("i=[" + i + "] j=[" + j +"]" );
    if ( cnt == 3 )
    {
      dtexto = dtexto + '.';
      j++;
      dtexto = dtexto + invertido.charAt(i);
      cnt = 1;
    }
    else
    {
      dtexto = dtexto + invertido.charAt(i);
      cnt++;
    }
  }

  invertido = "";
  for ( i=(dtexto.length-1),j=0; i>=0; i--,j++ )
    invertido = invertido + dtexto.charAt(i);

  window.document.getElementById("loginSystem-rut").value = invertido.toUpperCase()

  if ( revisarDigito(texto) )
    return true;

  return false;
}

function revisarDigito2( dvr )
{
  dv = dvr + ""
  if ( dv != "" && dv != '0' && dv != '1' && dv != '2' && dv != '3' && dv != '4' && dv != '5' && dv != '6' && dv != '7' && dv != '8' && dv != '9' && dv != 'k'  && dv != 'K')
  {
    $("#buttonAceptarAlerta").css("display","inline");
    $("#textoModal").html("Debe ingresar un digito verificador válido");
    $('#modalAlertas').modal('show');
    $("#loginSystem-rut").css("border","1px solid red");
    window.document.getElementById("loginSystem-rut").focus();
    window.document.getElementById("loginSystem-rut").select();
    return false;
  }
  return true;
}

function revisarDigito( crut )
{
  largo = crut.length;
  if(crut == ""){
    return false;
  }
  else if ( largo < 2)
  {
    $("#buttonAceptarAlerta").css("display","inline");
    $("#textoModal").html("Debe ingresar el rut completo");
    $('#modalAlertas').modal('show');
    window.document.getElementById("loginSystem-rut").value = "";
    $("#loginSystem-rut").css("border","1px solid red");
    window.document.getElementById("loginSystem-rut").focus();
    window.document.getElementById("loginSystem-rut").select();
    return false;
  }
  if ( largo > 2 )
    rut = crut.substring(0, largo - 1);
  else
    rut = crut.charAt(0);
  dv = crut.charAt(largo-1);
  revisarDigito2( dv );

  if ( rut == null || dv == null )
    return 0

  var dvr = '0'
  suma = 0
  mul  = 2

  for (i= rut.length -1 ; i >= 0; i--)
  {
    suma = suma + rut.charAt(i) * mul
    if (mul == 7)
      mul = 2
    else
      mul++
  }
  res = suma % 11
  if (res==1)
    dvr = 'k'
  else if (res==0)
    dvr = '0'
  else
  {
    dvi = 11-res
    dvr = dvi + ""
  }
  if ( dvr != dv.toLowerCase() )
  {
    $("#buttonAceptarAlerta").css("display","inline");
    $("#textoModal").html("Los datos ingresados no corresponden a un rut válido");
    $('#modalAlertas').modal('show');
    window.document.getElementById("loginSystem-rut").value = "";
    $("#loginSystem-rut").css("border","1px solid red");
    window.document.getElementById("loginSystem-rut").focus();
    window.document.getElementById("loginSystem-rut").select();
    return false
  }

  return true
}

Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};

//Cerrar sesion
$("#buttonLogout").unbind('click').click(function(){
  console.log("ok");
  $.ajax({
      url:   'controller/cerraSesion.php',
      type:  'post',
      success: function (response) {
        $.ajax({
          url:   'controller/datosAreas.php',
          type:  'post',
          success: function (response2) {
            var p2 = jQuery.parseJSON(response2);
            if(p2.aaData.length !== 0){
              for(var i = 0; i < p2.aaData.length; i++){
                $('div[id *=' + p2.aaData[i].PADRE + ']').css("display","none");
                $('li[id *=' + p2.aaData[i].NOMBRE + ']').css("display","none");
              }

              $("#menu-lateral").css("background","rgba(30, 0, 0, 0.0)");
              $("#logoMenu").css("color","black");
              $("#iconoLogoMenu").css("border","1px solid #b5b5b5");
              $("#iconoLogoMenu").css("background","rgba(255, 255, 255, 1.0)");
              window.location.href = "#/home";
            }
          }
        });
      }
  });
});

$("#buttonEnviar").unbind("click").click(function(event){
  event.preventDefault();
  event.stopImmediatePropagation();
  if($("#sintomasMail").val() === '' || $("#sintomasTelefono").val() === ''){
    $("#modalAlertas").modal({backdrop: 'static', keyboard: false});
    $("#buttonAceptarAlerta").css("display","inline");
    $("#textoModal").html("Favor complete los campos obligatorios");
    $('#modalAlertas').modal('show');
    $("#sintomasMail").css("border","1px solid red");
    $("#sintomasTelefono").css("border","1px solid red");
  }
  else if($("#sintomasTemperatura").val() === '0'){
    $("#modalAlertas").modal({backdrop: 'static', keyboard: false});
    $("#buttonAceptarAlerta").css("display","inline");
    $("#textoModal").html("Favor ingrese su temperatura");
    $('#modalAlertas').modal('show');
  }
  else{
    if($("#sintomasFotoId").val() !== ''){
      var d = $("#formImagen");

      formdata = new FormData();
      // En el formdata colocamos todos los archivos que vamos a subir
      for (var i = 0; i < (d.find('input[type=file]').length); i++) {
          // buscará todos los input con el valor "file" y subirá cada archivo. Serán diferenciados en el PHP gracias al "name" de cada uno.
        formdata.append((d.find('input[type="file"]').eq(i).attr("name")),((d.find('input[type="file"]:eq('+i+')')[0]).files[0]));
      }

      formdata.append('rut',$("#sintomasRut").val());
      formdata.append('mail',$("#sintomasMail").val());
      formdata.append('fono',$("#sintomasTelefono").val());
      formdata.append('temperatura',$("#sintomasTemperatura").val());
      formdata.append('tiempoIngreso',$("#tiempoIngreso").html());

      setTimeout(function(){
        $.ajax({
          url:   'controller/ingresaSintomas.php',
          type:  'post',
          data:  formdata,
          cache: false,
          contentType: false,
          processData: false,
          beforeSend: function(){
            $("#buttonAceptarAlerta").css("display","none");
            $("#modalAlertasSplash").modal({backdrop: 'static', keyboard: false});
            $("#textoModalSplash").html("<img src='view/img/loading.gif' class='splash_charge_logo'><font style='font-size: 12pt;'>Cargando</font>");
          },
          success:  function (response) {
            if(response !== "Error"){
              parametros2 = {
                "id": response,
                "sintomas": $("#sintomasMulti").val(),
                "temperatura": $("#sintomasTemperatura").val()
              }
              $.ajax({
                url:   'controller/sms_corona.php',
                type:  'post',
                data:  parametros2,
                success:  function (response) {
                }
              });
              $.ajax({
                url:   'controller/sms_corona.php',
                type:  'post',
                data:  parametros2,
                success:  function (response) {
                }
              });
              $.ajax({
                url:   'controller/ingresaSintomasMulti.php',
                type:  'post',
                data:  parametros2,
                success:  function (response) {
                  setTimeout(function(){
                    $('#modalAlertasSplash').modal('hide');
                  },500);
                  $("#buttonAceptarAlerta").css("display","inline");
                  $('#modalAlertas').modal({backdrop: 'static', keyboard: false});
                  $("#textoModal").html("<img src='view/img/ticket.png' class='splash_load'><br/>Ingresado correctamente");
                  $('input[id ^= sintomas]').attr("disabled","disabled");
                  $('number[id ^= sintomas]').attr("disabled","disabled");
                  $('select[id ^= sintomas]').attr("disabled","disabled");
                  $("#buttonEnviar").attr("disabled","disabled");
                }
              });
            }
            else{
              setTimeout(function(){
                $('#modalAlertasSplash').modal('hide');
              },500);
              $("#buttonAceptarAlerta").css("display","inline");
              $('#modalAlertas').modal({backdrop: 'static', keyboard: false});
              $("#textoModal").html("<img src='view/img/error.png' class='splash_load'><br/>Error en el ingreso si el problema persiste<br/>comuniquese al e-mail soporte@e-gestiontech.cl");
              $("#modalAlertas").modal("show");
            }
          }
        });
      },500);
    }
    else{
      setTimeout(function(){
        parametros = {
          "rut": $("#sintomasRut").val(),
          "mail": $("#sintomasMail").val(),
          "fono": $("#sintomasTelefono").val(),
          "temperatura": $("#sintomasTemperatura").val(),
          "tiempoIngreso": $("#tiempoIngreso").html(),
          "foto": ''
        }
        $.ajax({
          url:   'controller/ingresaSintomas.php',
          type:  'post',
          data:  parametros,
          beforeSend: function(){
            $("#buttonAceptarAlerta").css("display","none");
            $("#modalAlertasSplash").modal({backdrop: 'static', keyboard: false});
            $("#textoModalSplash").html("<img src='view/img/loading.gif' class='splash_charge_logo'><font style='font-size: 12pt;'>Cargando</font>");
          },
          success:  function (response) {
            if(response !== "Error"){
              parametros2 = {
                "id": response,
                "sintomas": $("#sintomasMulti").val(),
                "temperatura": $("#sintomasTemperatura").val()
              }
              $.ajax({
                url:   'controller/sms_corona.php',
                type:  'post',
                data:  parametros2,
                success:  function (response) {
                }
              });
              $.ajax({
                url:   'controller/ingresaSintomasMulti.php',
                type:  'post',
                data:  parametros2,
                success:  function (response) {
                  setTimeout(function(){
                    $('#modalAlertasSplash').modal('hide');
                  },500);
                  $("#buttonAceptarAlerta").css("display","inline");
                  $('#modalAlertas').modal({backdrop: 'static', keyboard: false});
                  $("#textoModal").html("<img src='view/img/ticket.png' class='splash_load'><br/>Ingresado correctamente");
                  $('input[id ^= sintomas]').attr("disabled","disabled");
                  $('number[id ^= sintomas]').attr("disabled","disabled");
                  $('select[id ^= sintomas]').attr("disabled","disabled");
                  $("#buttonEnviar").attr("disabled","disabled");
                }
              });
            }
            else{
              setTimeout(function(){
                $('#modalAlertasSplash').modal('hide');
              },500);
              $("#buttonAceptarAlerta").css("display","inline");
              $('#modalAlertas').modal({backdrop: 'static', keyboard: false});
              $("#textoModal").html("<img src='view/img/error.png' class='splash_load'><br/>Error en el ingreso si el problema persiste<br/>comuniquese al e-mail soporte@e-gestiontech.cl");
            }
          }
        });
      },500);
    }
  }
});

$('input[id ^= sintomas]').on('input', function(){
  $('input[id ^= sintomas]').css("border","");
  $('number[id ^= sintomas]').css("border","");
});

$('number[id ^= sintomas]').on('input', function(){
  $('input[id ^= sintomas]').css("border","");
  $('number[id ^= sintomas]').css("border","");
});

$("#buttonSalir").unbind('click').click(function(){
  window.location.href = "#/home";
  $("#logoLinkWeb").fadeOut();
  $("#logoMenu").fadeOut();
  $("#lineaMenu").fadeOut();
  $("#iconoLogoMenu").attr("class","imgMenu fas fas fa-bars");
});

$("#sintomasTelefono").on('input', function () {
    this.value = this.value.replace(/[^0-9,.]/g,'');
});

$('#sintomasFoto').on('change', function(e){
  var file = e.target.files[0].name;
  $("#sintomasFotoId").val(file);

  var reader = new FileReader();
  reader.readAsDataURL(e.target.files[0]);
  setTimeout(function(){
    $("#divImgTermometro").css("display","inline");
    $("#imgTermometro").attr("src",reader.result);
    $("#divImgTermometro2").css("display","inline");
    $("#imgTermometro2").css("height",$("#divEppAst").height());
    $("#imgTermometro2").attr("src",reader.result);
  },500)
});

$("#openSintomas").unbind('click').click(function(){
  $('ul[class ^= touchMultiSelect]').toggleClass('opened');
  if($("#iconOpenSintomas").attr('class') == 'fas fa-arrow-down'){
    $("#iconOpenSintomas").attr('class','fas fa-arrow-up');
  }
  else{
    $("#iconOpenSintomas").attr('class','fas fa-arrow-down');
  }
});
