var app = angular.module("WPApp", ["ngRoute"]);
$("#menu-lateral").css("width","45px");

app.config(function($routeProvider, $locationProvider) {
    $routeProvider
    .when("/home", {
        controller: "homeController",
        controllerAs: "vm",
        templateUrl : "view/home/home.html"
    })
    .when("/logged", {
        controller: "loggedController",
        controllerAs: "vm",
        templateUrl : "view/home/logged.html"
    })
    .otherwise({redirectTo: '/home'});

    $locationProvider.hashPrefix('');
});

app.controller("homeController", function(){
    $("#modalAlertasSplash").modal({backdrop: 'static', keyboard: false});
    $("#textoModalSplash").html("<img src='view/img/loading.gif' class='splash_charge_logo'><font style='font-size: 12pt;'>Cargando</font>");
    $('#modalAlertasSplash').modal('show');
    // $.ajax({
    //   url:   'controller/limpia_session.php',
    //   type:  'post',
    //   success:  function (response) {
    //
    //   }
    // });
    setTimeout(function(){
        $('#contenido').fadeIn();
        $('#header').fadeIn();
        $('#footer').fadeIn();
        $('#menu-lateral').fadeIn();
        $("#menu-lateral").css("width","45px");
        // $('#menu-lateral').hover(function(){
        //     $("#menu-lateral").css("width","200px");
        // },
        // function() {
        //     $("#menu-lateral").css("width","45px");
        // });
        setTimeout(function(){
          $('#modalAlertasSplash').modal('hide');
        },500);
        $("#loginSystem").show("slide", {direction: "up"}, 800);
    },1500);
});

app.controller("loggedController", function(){
  $("#modalAlertasSplash").modal({backdrop: 'static', keyboard: false});
  $("#textoModalSplash").html("<img src='view/img/loading.gif' class='splash_charge_logo'><font style='font-size: 12pt;'>Cargando</font>");
  $('#modalAlertasSplash').modal('show');
  if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
      $(".navbar-brand").css("height","40pt");
      $(".navbar-toggle").fadeIn();
      $("#tituloIngresoSintomas").css("margin-right","0pt");
      $("#tituloIngresoSintomas").parent().css("text-align","center");
  }
  else{
    $(".navbar-brand").css("height","65pt");
    $("#login").height($(window).height() - 150);
    $("#sintomasTemperatura").select2({
        theme: "bootstrap"
    });
    $("#tituloIngresoSintomas").css("margin-right","10pt");
    $("#tituloIngresoSintomas").parent().css("text-align","right");
  }
  $.ajax({
      url:   'controller/datosAccesoPost.php',
      type:  'post',
      beforeSend: function(){
          $("#buttonAceptarAlerta").css("display","none");
          $('#modalAlertasSplash').modal({backdrop: 'static', keyboard: false});
          $("#textoModalSplash").html("<img src='view/img/loading.gif' class='splash_charge_logo'><font style='font-size: 12pt;'>Cargando</font>");
          $('#modalAlertasSplash').modal('show');
      },
      success:  function (response) {
        var p = jQuery.parseJSON(response);
        if(p.aaData.length !== 0){
          $("#sintomasRut").val(p.aaData[0].DNI);
          $("#sintomasNombre").val(p.aaData[0].NOMBRE);
          $("#sintomasTelefono").val(p.aaData[0].FONO);
          $("#sintomasMail").val(p.aaData[0].CORREO);
          $("#tiempoIngreso").html(p.aaData[0].HH);
          $.ajax({
            url:   'controller/datosSintomas.php',
            type:  'post',
            success:  function (response) {
              var p = jQuery.parseJSON(response);
              if(p.aaData.length !== 0){
                var cuerpoSintomas = "";
                for(var i = 0; i < p.aaData.length; i++){
                  cuerpoSintomas += "<option value=" + p.aaData[i].IDSINTOMAS + ">" + p.aaData[i].NOMBRE + "</option>";
                }
                $("#sintomasMulti").html(cuerpoSintomas);
                // $("#sintomasMulti").multiSelect({
                //   selectableHeader: "<div class='custom-header'>&nbsp;Disponibles</div>",
                //   selectionHeader: "<div class='custom-header'>&nbsp;Seleccionadas</div>"
                // });
                $("#sintomasMulti").touchMultiSelect();
                var cuerpoTemperatura = '';
                var grado = 32;
                cuerpoTemperatura += "<option selected value=0></option>";
                while(grado < 40.0){
                  cuerpoTemperatura += "<option value=" + (grado).toFixed(1) + ">" + grado.toFixed(1) + "</option>";
                  grado = grado + .1;
                }
                cuerpoTemperatura += "<option value=" + (grado).toFixed(1) + ">" + grado.toFixed(1) + "</option>";
                $("#sintomasTemperatura").html(cuerpoTemperatura);
                setTimeout(function(){
                    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                        $(".navbar-brand").css("height","40pt");
                        $(".navbar-toggle").fadeIn();
                    }
                    else{
                      $(".navbar-brand").css("height","65pt");
                      $("#login").height($(window).height() - 150);
                    }
                    $('#contenido').fadeIn();
                    $('#menu-lateral').fadeIn();
                    $('#footer').fadeIn();
                    setTimeout(function(){
                      $('#modalAlertasSplash').modal('hide');
                    },1000);
                },1500);
              }
            }
          });
        }
      }
  });
  new ResizeSensor(jQuery("#divEppAst"), function(){
    if($(window).width() < 700){
      $(".touchMultiSelect").css("width","100%");
    }
    else if($(window).width() < 1024){
      $(".touchMultiSelect").css("width",$(window).width()/2);
    }
    else{
      $(".touchMultiSelect").css("width",$(window).width()/4);
    }
  });
});
