<?php
	header('Access-Control-Allow-Origin: *');
  ini_set('display_errors', 'Off');
	require('../model/consultas.php');
	session_start();

	if(count($_POST) > 0){
		$rut = $_POST['rutUser'];
		$row = consultasAcceso($rut);
    $_SESSION['rutUser'] = $rut;

  	if(is_array($row)){
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($row),
            "iTotalDisplayRecords" => count($row),
            "aaData"=>$row
        );
        echo json_encode($results);
    }
    else{
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => 0,
            "iTotalDisplayRecords" => 0,
            "aaData"=>[]
        );
        echo json_encode($results);
    }
	}
	else{
		echo false;
	}
?>
