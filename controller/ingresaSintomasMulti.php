<?php
    ini_set('display_errors', 'Off');
    require('../model/consultas.php');
    require("phpmailer/PHPMailerAutoload.php");
    include('simple_html_dom.php');
    date_default_timezone_set('America/Santiago');
    session_start();
	if(count($_POST) > 0){
        $id = $_POST['id'];
        $sintomas = $_POST['sintomas'];
        $temperatura = $_POST['temperatura'];

        $row = '';

        if($sintomas[0] !== ''){
            for($i = 0; $i < count($sintomas); $i++){
                $id_sintoma = $sintomas[$i];
                $row =  ingresaSintomasMulti($id,$id_sintoma);
            }

            if($temperatura > 37.00){
                $correo = selectSintomasCorreo($id);

                $dias = array("Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado");
                $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");

                $fecha = strtotime('-0 day');
                $fecha = $dias[date('w', $fecha)]." ".date('d', $fecha)." de ".$meses[date('n', $fecha)-1]. " ".date('Y', $fecha) . " a las " .date("H:i:s") . " hrs.";

                $mail = new PHPMailer(); // defaults to using php "mail()"

                //Codificacion
                $mail->CharSet = 'UTF-8';

                //indico a la clase que use SMTP
                $mail->SMTPSecure = 'tls';
                $mail->Host = "smtp.gmail.com"; // GMail
                $mail->Port = 587;
                $mail->IsSMTP(); // use SMTP
                $mail->SMTPAuth = true;
                //indico un usuario / clave de un usuario
                $mail->Username = "contacto@e-gestiontech.cl";
                $mail->Password = "e-gestion2020@";

                // $mail->SMTPOptions = array(
                //     'ssl' => array(
                //         'verify_peer' => false,
                //         'verify_peer_name' => false,
                //         'allow_self_signed' => true
                //     )
                // );

                $firma = "
                <p style='margin-top: -10px; font-family: Arial;font-size:9pt;'>
                                <b><font style='color:rgb(0,58,255);'>Alertas E-GestionTech</font></b>
                <br>
                                <a href='mailto:contacto@e-gestiontech.cl' style='text-decoration:none;color:rgb(84,88,90);'><font style='color:rgb(0,58,255);'>contacto@e-gestiontech.cl</font></a>
                <br>
                <a href='http://www.e-gestiontech.cl' target='_blank'><font style='color:rgb(0,58,255);'>www.e-gestiontech.cl</font></a>
                <br>
                <font style='color:rgb(84,88,90);'>..........................................................................................................................................................................</font>
                <br>
                <b><font style='color:rgb(0,58,255);'>E-GestionTech</font></b>
                <br>
                <font style='color:rgb(84,88,90);'>
                Morande 835
                <br>
                Comuna Santiago – Santiago - Chile <br />
                <br>
                ..........................................................................................................................................................................
                <br>
                AVISO LEGAL.
                <p style='line-height: 15px;font-family: Arial;font-size:7.5pt; text-align: justify; width: 100%'>
                Este mensaje y sus documentos anexos pueden contener información confidencial o legalmente protegida. Está dirigido única y exclusivamente a la persona o entidad reseñada como destinatarios del mensaje. Si este mensaje le hubiera llegado por error, por favor elimínelo sin revisarlo ni reenviarlo y notifíquelo lo antes posible al remitente. Cualquier divulgación, copia o utilización de dicha información es contraria a la ley. Le agradecemos su colaboración.
                </p>
                </font>
                <br>
                </p>
                ";

                $body = "
                        <div style='width: 100%; text-align: justify; margin: 0 auto;'>
                        <font style='font-size: 14px;'>
                        Estimado,
                        <br />
                        <br />
                        le informamos que el colaborador <b>" . $correo[0]['NOMBRE'] . "</b> ha indicado sintomas que pueden significar contagio por <b>coronavirus</b>, favor tomar las medidas de prevención necesarias:
                        <br />
                        <br />
                        Datos:
                        <br />
                        <br />
                        RUT: " . $correo[0]['RUT'] . "<br />
                        Nombre: " . $correo[0]['NOMBRE'] . "<br />
                        Temperatura (°C): " . $correo[0]['TEMP'] . "<br />
                        Tos: " . $correo[0]['TOS'] . "<br />
                        Dificultad para respirar: " . $correo[0]['DIF_RESPIRAR'] . "<br />
                        Dolor muscular: " . $correo[0]['DOL_MUSC'] . "<br />
                        Dolor de garganta: " . $correo[0]['DOL_GAR'] . "<br />
                        Secrecion nasal: " . $correo[0]['SEC_NASAL'] . "<br />
                        Manchas en la piel: " . $correo[0]['MAN_PIEL'] . "<br />
                        Dolor de cabeza: " . $correo[0]['DOLOR_CAB'] . "<br />
                        Náuseas / vómito: " . $correo[0]['NAU_VOM'] . "<br />
                        Dolor articular: " . $correo[0]['DOLOR_ART'] . "<br />
                        DNI jefatura: " . $correo[0]['DNI_JEFE'] . "<br />
                        Nombre jefatura: " . $correo[0]['NOMBRE_JEFE'] . "<br />
                        E-Mail Jefatura: " . $correo[0]['MAIL_JEFE'] . "<br />
                        <br />
                        <br />
                        </font>
                        </div>
                        <div'>
                            <font style='font-size: 14px;'>
                                Saludos cordiales.
                            </font>
                            <br />
                            <br />
                            <br />
                            <br />
                            " . $firma . "
                        </div>
                        ";

                $mail->SetFrom('contacto@e-gestiontech.cl', "Alertas E-GestionTech");

                //defino la dirección de email de "reply", a la que responder los mensajes
                //Obs: es bueno dejar la misma dirección que el From, para no caer en spam
                $mail->AddReplyTo("contacto@e-gestiontech.cl", "Alertas E-GestionTech");

                $datosMail = selectPersonalEncargadoSintomas();

                for($z = 0; $z < count($datosMail); $z++){
                    if($datosMail[$z]['TIPO'] === 'directo'){
                        $mail->AddAddress($datosMail[$z]['MAIL'],$datosMail[$z]['MAIL']);
                    }
                    if($datosMail[$z]['TIPO'] === 'copia_oculta'){
                        $mail->addBCC($datosMail[$z]['MAIL'],$datosMail[$z]['MAIL']);
                    }
                }

                $mail->AddAddress($correo[0]['MAIL_JEFE'],$correo[0]['MAIL_JEFE']);

                $mail->Subject = "Información sintomas colaborador " . $correo[0]['RUT'] . " " . $fecha;

                $mail->MsgHTML($body);

                if(!$mail->Send()) {
                    echo 'Message could not be sent.';
                    echo 'Mailer Error: ' . $mail->ErrorInfo;
                } else {
                    echo 'Message has been sent';
                }

                // var_dump($dato);
            }
        }
	}
	else{
		echo "Sin datos";
	}
?>
