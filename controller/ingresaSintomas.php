<?php
    ini_set('display_errors', 'Off');
    require('../model/consultas.php');
    require("phpmailer/PHPMailerAutoload.php");
    // include('simple_html_dom.php');
    date_default_timezone_set('America/Santiago');
    session_start();
	if(count($_POST) > 0){
      $rut = $_POST['rut'];
      $mail = $_POST['mail'];
      $fono = $_POST['fono'];
      $temperatura = $_POST['temperatura'];
      $tiempoIngreso = $_POST['tiempoIngreso'];

      if($rut == '13913245-9'){
          borraRegistroPrevio($rut,$tiempoIngreso);
          borraRegistroPrevio2($rut,$tiempoIngreso);
      }

      $revisar = getimagesize($_FILES["sintomasFoto"]["tmp_name"]);
      if($revisar !== false){
        $medidasimagen= getimagesize($_FILES['sintomasFoto']['tmp_name']);
        if($medidasimagen[0] < 1280 && $_FILES['sintomasFoto']['size'] < 100000){
          $image = $_FILES['sintomasFoto']['tmp_name'];
          $imgContenido = addslashes(file_get_contents($image));
          $row = ingresaSintomas($rut,$mail,$fono,$temperatura,$tiempoIngreso,$imgContenido);
        }
        else{
          $nombrearchivo=$_FILES['sintomasFoto']['name'];
          $rtOriginal=$_FILES['sintomasFoto']['tmp_name'];
          if($_FILES['sintomasFoto']['type']=='image/jpeg'){
            $original = imagecreatefromjpeg($rtOriginal);
          }
          else if($_FILES['sintomasFoto']['type']=='image/png'){
            $original = imagecreatefrompng($rtOriginal);
          }
          list($ancho,$alto)=getimagesize($rtOriginal);
          $max_ancho = 600;
          $max_alto = 800;
          $x_ratio = $max_ancho / $ancho;
          $y_ratio = $max_alto / $alto;
          if( ($ancho <= $max_ancho) && ($alto <= $max_alto) ){
              $ancho_final = $ancho;
              $alto_final = $alto;
          }
          elseif (($x_ratio * $alto) < $max_alto){
              $alto_final = ceil($x_ratio * $alto);
              $ancho_final = $max_ancho;
          }
          else{
              $ancho_final = ceil($y_ratio * $ancho);
              $alto_final = $max_alto;
          }
          $lienzo=imagecreatetruecolor($ancho_final,$alto_final);
          imagecopyresampled($lienzo,$original,0,0,0,0,$ancho_final, $alto_final,$ancho,$alto);
          $hotpink = imagecolorallocate($image, 255, 110, 221);
          $lienzo2 = imagerotate($lienzo, 270, $hotpink);
          if($_FILES['sintomasFoto']['type']=='image/jpeg'){
            imagejpeg($lienzo2,$_FILES['sintomasFoto']['tmp_name']);
          }
          else if($_FILES['sintomasFoto']['type']=='image/png'){
            imagepng($lienzo2,$_FILES['sintomasFoto']['tmp_name']);
          }
          $image = $_FILES['sintomasFoto']['tmp_name'];
          $imgContenido = addslashes(file_get_contents($image));
          $row = ingresaSintomas($rut,$mail,$fono,$temperatura,$tiempoIngreso,$imgContenido);
        }
      }
      else {
        $row = ingresaSintomasSinFoto($rut,$mail,$fono,$temperatura,$tiempoIngreso);
      }

      if($row !== "Error"){
        echo $row->insert_id;
      }
      else{
        echo "Error";
      }
	}
	else{
		echo "Sin datos";
	}
?>
